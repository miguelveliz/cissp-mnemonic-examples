## ISC2 Ethics Canons

PLOP
**P**eople
**L**egally
**O**rganization
**P**rofession



## Threat Modeling

STRIDE - Microsoft threat modeling tool
- **S**poofing
- **T**ampering
- **R**epudiation - attacker can deny participation
- **I**nformation disclosure
- **D**enial of service
- **E**levation of privilege

DREAD
- **D**amage
- **R**eproducibility 
- **E**xploitability 
- **A**ffected users 
- **D**iscoverability


## Ring computing model

Zero KODU
- 0 **K**ernal           
- 1 **O**perating System 
- 2 **D**rivers          
- 3 **U**ser             


## Symmetric Encryption

23BRAIDS  
- **Two**fish 
- **3**DES 
- **B**lowfish 
- **R**C5 
- **A**ES 
- **I**DEA 
- **D**ES 
- **S**kipjack

## Asymmetric Encryption

DEREK (jeter)
- **D**iffie-Hellman
- **E**lliptic Curve 
- **R**SA
- **E**l Gamal 
- **K**napsack

## Hash Functions

M@SHH!T (skip non letters)  
- **M**D5 
- **S**HA-1/2/3 
- **H**MAC 
- **H**AVAL
- **T**iger



## OSI Model

Please Do Not Throw Sausage Pizza Away
- **P**hysical
- **D**ata Link
- **N**Network
- **T**ransport
- **S**ession
- **P**resentation
- **A**pplication

SPF10 (1,0 Bits)
-  **S**egments
-  **P**ackets
-  **F**rames
-  **B**Bits


## TCP/IP Model

NITA
- **N**etwork Access
- **I**nternet
- **T**ransport
- **A**pplication

TCP/IP comes in **A TIN**




## Fire Extinguisher Classes

- A - **A**sh -> Combustible
- B - **B**oil -> Liquid
- C - **C**urrent -> Electrical
- D - **D**ent -> Metal
- K - **K**itchen -> Oil/Fat






## MISC

Type 2 errors are FAR away.
- FAR = Type 2
- FRR = Type 1

PGP is a good IDEA.
- PGP <-> IDEA



## PENDING

## Incident Response

People in Canada Eat Raw Lettuce  
- **P**repare
- **I**dentify 
- **C**ontain 
- **E**radicate 
- **R**ecovery 
- **L**essons learned


## BCP

Inguannas In Paris Really Cant Teach Me  
- **I**nitiation
- **I**mpact
- **P**reventative
- **R**ecovery
- **C**ontinuity
- **T**est
- **M**anage

## Risk Management

Beer (ALE) causes AROuSLE
- ALE = ARO * SLE